local preferences = inline:getDefaultSharedPreferences()
local enabled

local function watcher(input)
    if enabled then
        local text = input:getText()
        if text ~= nil and text.toString ~= nil then
            text = text:toString()
            if text:sub(#text) == "." then
                enabled = false
            else
                inline:setText(input, preferences:getString("spamtext", "spam"))
                input:refresh()
            end
        end
    end
end

local function spam(_, query)
    enabled = true
    inline:toast "Press dot - . to stop"
    query:answer()
end

local function setspamtext(_, query)
    preferences:edit():putString("spamtext", query:getArgs()):apply()
    query:answer "success"
end

return function(module)
    module:setCategory "Spam"
    module:registerCommand("spam", spam, "Semi-auto spammer")
    module:registerCommand("setspamtext", setspamtext, "Customize spam message")
    module:registerWatcher(watcher)
end