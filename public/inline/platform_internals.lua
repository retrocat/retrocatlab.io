local function createEnvironment()
    local environment = {}
    setmetatable(environment, {__index = _G})
    return environment
end

local function modifyEnvironment(environment, accessibilityNodeInfo, query)
    environment["query"] = query
    environment["accessibilityNodeInfo"] = accessibilityNodeInfo
end

local environment = createEnvironment()

local function eval(accessibilityNodeInfo, query)
    modifyEnvironment(environment, accessibilityNodeInfo, query)
    local chunk = load("return " .. query:getArgs(), "Platform internals", "t", environment)
    if chunk then
        query:answer(tostring(chunk()))
    end
end

local function exec(accessibilityNodeInfo, query)
    modifyEnvironment(environment, accessibilityNodeInfo, query)
    local chunk = load(query:getArgs(), "Platform internals", "t", environment)
    if chunk then
        local result = chunk()
        if result then
            query:answer(tostring(result))
        end
    end
end

local function recreate(_, query)
    environment = createEnvironment()
    query:answer()
end

local function freezeValue(data)
    local t = type(data)
    if t == "string" then
        return string.format('"%s"', data:gsub('"', '\\"'):gsub("\n", "\\n"))
    elseif t == "userdata" or t == "function" then
        return freezeValue({unfreezable = {[type(data)] = tostring(data)}})
    elseif t == "table" then
        local result = "{"
        for k, v in pairs(data) do
            result = result .. string.format("[%s]=%s,", freezeValue(k), freezeValue(v))
        end
        return result .. "}"
    else
        return tostring(data)
    end
end

local function freeze(_, query)
    local values = inline:getSharedPreferences("pl-" .. query:getArgs()):edit()
    values:clear()
    for k, v in pairs(environment) do
        values:putString(k, freezeValue(v))
    end
    values:apply()
    query:answer("Frozen!")
end

local function unfreeze(_, query)
    local iterator = inline:getSharedPreferences("pl-" .. query:getArgs()):getAll():entrySet():iterator()
    while iterator:hasNext() do
        local entry = iterator:next()
        environment[entry:getKey()] = load("return " .. entry:getValue())()
    end
    query:answer("Unfrozen!")
end

return function(module)
    module:setCategory("Lua")
    module:registerCommand("eval", eval, "Evaluates lua code")
    module:registerCommand("exec", exec, "Executes lua code")
    module:registerCommand("recreate", recreate, "Recreate environment")
    module:registerCommand("freeze", freeze, "Freezes the environment")
    module:registerCommand("unfreeze", unfreeze, "Unfreezes the environment")
end
