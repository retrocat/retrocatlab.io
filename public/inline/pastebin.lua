require "com.wavecat.inline.libs.http"

local preferences = inline:getDefaultSharedPreferences()

local function setpkey(_, query)
    preferences:edit():putString("pkey", query:getArgs()):apply()
    query:answer "Success!"
end

local function pastebin(_, query)
    local body =
        http.buildFormBody(
        {
            api_dev_key = preferences:getString("pkey", ""),
            api_option = "paste",
            api_paste_code = query:getArgs()
        }
    )
    request = http.Request.Builder.new():url("https://pastebin.com/api/api_post.php"):post(body):build()
    http.call(
        request,
        function(_, _, data)
            query:answer(data)
        end
    )
end

return function(module)
    module:setCategory("Pastebin")
    module:registerCommand("pbin", pastebin, "Create new paste with pastebin")
    module:registerCommand("setpkey", setpkey, "Sets the pastebin key")
end
