require "com.wavecat.inline.libs.http"

local function short(_, query)
    if query:getArgs() == "" then
        query:answer("empty url")
        return
    end

    local request =
        http.Request.Builder.new()
               :url(http.buildUrl("https://clck.ru/--", {url = query:getArgs()}))
               :get()
               :build()
    
    query:answer("loading")

    http.call(
        request,
        function(_, _, string)
            query:answer(string:sub(1, 4) == "http" and string or "can't shorten link")
        end
    )
end

return function(module)
    module:registerCommand("short", short, "Shorten any link with clck.ru")
end
