local preferences = inline:getDefaultSharedPreferences()

local url = "https://dmitrykotov.tk/inline/store/phrases.txt"
local default = "/store/phrases.txt"

local dirpath

local function check()
    local file = io.open(dirpath .. default, "r")
    if file then
        file:close()
    end
    return file
end

local function get()
    math.randomseed(os.time())
    local random = math.random(0, 5000)
    local lines = 0
    for line in io.lines(dirpath .. default) do
        lines = lines + 1
        if lines == random then
            return line
        end
    end
end

local function getcached()
    local result = preferences:getString("bulling", "")
    return result == "" and get() or result
end

local function cache()
    preferences:edit():putString("bulling", get()):apply()
end

local function bull(_, query)
    if not check() then
        local request = http.Request.Builder.new():url(url):get():build()
        query:answer("Downloading...")
        http.call(
            request,
            function(_, response, string)
                os.execute("mkdir " .. dirpath .. "/store")
                local file = io.open(dirpath .. default, "w+")
                file:write(string)
                file:close()
                query:answer(getcached())
                cache()
            end,
            function(_, exception)
                query:answer("Error while downloading phrases")
            end
        )
        return
    end
    query:answer(getcached())
    cache()
end

return function(module)
    local path = module:getFilepath()
    dirpath = path:sub(1, path:match("^.*()/"))

    module:registerCommand("bull", bull, check() and getcached() or "")
end
