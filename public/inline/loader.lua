require "com.wavecat.inline.libs.http"
require "com.wavecat.inline.libs.colorama"

local dirpath

colorama.init(inline)

local function loadcmd(input, query)
    local request = http.Request.Builder.new()
                                      :url(query:getArgs())
                                      :get()
                                      :build()
    
    query:answer(colorama.font("Loading...", "#8AB4F8"))
    
    http.call(
        request,
        function(_, response, string)
            local chunk, err = load(string)
            
            if not chunk then
                query:answer(colorama.font("Unable to load module: " .. err, "#FF7373"))
                return
            end

            local filename = query:getArgs():sub(query:getArgs():match("^.*()/") + 1, #query:getArgs())
            local file = io.open(dirpath .. filename, "w+")
            file:write(string)
            file:close()
            
            query:answer(colorama.font("Module installed: " .. filename, "#D7FFD5"))
            inline:createEnvironment()
        end,
        function(_, exception)
            query:answer(colorama.font("Unable to download module: " .. exception:getMessage(), "#FF7373"))
        end
    )
end

return function(module)
    module:setCategory "Loader"
    module:registerCommand("load", colorama.wrap(loadcmd), "Load module by url")

    local path = module:getFilepath()
    dirpath = path:sub(1, path:match("^.*()/"))
end
